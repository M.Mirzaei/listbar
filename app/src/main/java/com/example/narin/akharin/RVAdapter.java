package com.example.narin.akharin;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.kalaViewHolder> {


    public  class kalaViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView kalaName;
        TextView kala_sub;
        ImageView kalaPhoto;

        kalaViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            kalaName = (TextView) itemView.findViewById(R.id.item_title);
            kala_sub = (TextView) itemView.findViewById(R.id.item_detail);
            kalaPhoto = (ImageView) itemView.findViewById(R.id.item_image);
        }
    }
    @Override
    public kalaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
       View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_layout,viewGroup, false);
       kalaViewHolder kvh = new kalaViewHolder(v);
        return kvh;
    }

    @Override
    public  void onBindViewHolder(kalaViewHolder holder, int i) {
        kala kala = this.kala.get(i);

        holder.kalaName.setText(kala.name);
        holder.kala_sub.setText(kala.sub_cat);
        holder.kalaPhoto.setImageResource(kala.photoId);

    }

    @Override
    public int getItemCount() {

        return kala.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }





    List<kala> kala;

    RVAdapter(List<kala> kala){
        this.kala = kala;
    }




}

