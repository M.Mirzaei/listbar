package com.example.narin.akharin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public List<kala> kala;

    public void initializeData() {
        kala = new ArrayList<>();
        kala.add(new kala("آرد", "غلات و حبوبات", R.drawable.ard));
        kala.add(new kala("ذغال سنگ", "سنگ،فلزات و مواد معدنی", R.drawable.zoghal));
        kala.add(new kala("موزاییک", "سنگ،فلزات و مواد معدنی", R.drawable.moozaeek));
        kala.add(new kala("مواد شوینده", "محصولات آرایشی و بهداشتی", R.drawable.shooyande));
        kala.add(new kala("کاشی", "سنگ،فلزات و مواد معدنی", R.drawable.kashi));
        kala.add(new kala("ضایعات برنج", "سنگ،فلزات و مواد معدنی", R.drawable.berenj));
        kala.add(new kala("ایزوگام", "مایعات، گازها و محصولات شیمیایی", R.drawable.isogam));
        kala.add(new kala("کاشی", "سنگ،فلزات و مواد معدنی", R.drawable.kashi_ahvaz));
        kala.add(new kala("کاشی", "سنگ،فلزات و مواد معدنی", R.drawable.kashi_ahvaz));
        kala.add(new kala("کاشی", "سنگ،فلزات و مواد معدنی", R.drawable.kashi_ahvaz));


    }


    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeData();


        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        RVAdapter adapter = new RVAdapter(kala);
        rv.setAdapter(adapter);

    }


}
